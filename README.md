# Application Blueprint for MediaWiki

## What is MediaWiki?

MediaWiki is free and open-source wiki software used by thousands of websites, companies, and organizations.

**Official Website:** [mediawiki.org](https://www.mediawiki.org)


This application blueprint uses the [bitnami mediawiki](https://hub.docker.com/r/bitnami/mediawiki) Docker image.

## Filling out a deployment blueprint
For the general steps to deploy an application with Unfurl Cloud, see our [Getting Started guides](https://unfurl.cloud/help#guides).

### Details
To simplify the deployment process, we have set default values for several required environment variables and hidden them from the UI. The remaining environment variables are exposed as the following “Detail” fields, which you must fill in after selecting a deployment:

| Input Name                    | Default Value    |
|-------------------------------|------------------|
| Host                          | `<subdomain>.<your-zone>`|
| User                          | (none)           |
| Password                      | (none)           |
| Email                         | (none)           |
| Wiki Name                     | (none)           |
| Database Name                 | (none)           |
| Database User                 | (none)           |
| Database Password             | (none)           |
| SMTP User                     | (none)           |
| SMTP Password                 | (none)           |

Below is a list of the hidden default environment variables and their values. As explained above, you will not encounter these environment variables as you fill in the deployment blueprint:

| Input Name                    | Default Value    |
|-------------------------------|------------------|
| Enable HTTPS                  | `true`           |
| Database Host                 | (auto-generated) |
| Database Port Number          | (depending on Database)|
| SMTP Port                     | (depending on Mail Server) |


### Components
Each application blueprint includes **components**. These are the required and optional resources for the application. In most cases, there is more than one way to fulfill a component requirement. After you select a deployment blueprint, you will be prompted to fulfill the component requirements and configure the deployment to your needs. Common components include Compute, DNS, Database, and Mail Server:

#### Compute

MediaWiki requires the following compute resources:

- At least 1 CPU
- At least 512MB of RAM
- At least 16GB of hard disk space

#### DNS

A DNS provider must be specified for the deployed site to be accessible via a domain name.

Supported DNS providers are:

- Google Cloud DNS
- DigitalOcean DNS
- AWS Route53
- Azure DNS

All providers require a domain name to use. ***Note:** the domain must be registered to that service.*

The `Subdomain` input above will be used to register a new subdomain under the given domain. For example, given the subdomain `wiki` and domain zone `mysite.com`, the site will be accessible at `wiki.mysite.com`.

#### Database: MySQL or MariaDB

For this blueprint, MediaWiki can use either of these databases:

| Database | Supported Versions |
|----------|--------------------|
| MySQL    | `5.7+`       |
| MariaDB  | `10.3+`      |

For additional compatibility information, see [MediaWiki's compatibility page](https://www.mediawiki.org/wiki/Compatibility).

#### Mail Server (optional extra)

MediaWiki optionally requires a mail server to send out emails.

Two mail providers are supported, with their needed inputs detailed below:

<!-- use <br> for multi-line tables, GFM does not support multi-line tables -->

**SendGrid:**

| Input   | Description                                                                                    |
|---------|------------------------------------------------------------------------------------------------|
| API Key | This can be created at [SendGrid account settings](https://app.sendgrid.com/settings/api_keys) |

**Generic SMTP Server:**

| Input     | Description                                                                     |
|-----------|---------------------------------------------------------------------------------|
| SMTP Host | Name of the mail server, e.g. `smtp.someprovider.com`                           |
| User Name | Usually the email to send as. Double check the documentation for your provider! |
| Secret    | The password or login token to authenticate with.                               |
| Protocol  | Either `ssl` or `tls`, probably `tls`.                                          |

**Gmail:**

Use Generic SMTP Server with the following inputs:

| Input     | Value                                       |
|-----------|---------------------------------------------|
| SMTP Host | `smtp.gmail.com`                            |
| User Name | Gmail username (email without `@gmail.com`) |
| Secret    | Gmail password                              |
| Protocol  | `tls`                                       |

#### Required vs “Extras”
Some components are required for a deployment to succeed. Others, found under the “Extras” tab, are optional resources that enhance the features or performance of your deployment.
